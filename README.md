# JsonFormatter.php

Useful class for formatting entities, arrays, etc. into JSON

To configure in services.yml:

    app.jsonformatter:
	  class: CYINT\ComponentsPHP\Services\JsonFormatterService
	  arguments: ["@doctrine"]
	  calls:
	 	 - [ setContainer, ["@service_container"] ]
