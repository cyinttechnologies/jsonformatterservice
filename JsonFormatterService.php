<?php
/*JsonFormatterService.php
Symfony service to convert entities or array collections to arrays in preparation for json formatting
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



namespace CYINT\ComponentsPHP\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;

class JsonFormatterService implements ContainerAwareInterface
{
    protected $Doctrine;

    public function __construct(Registry $Doctrine)
    {
        $this->Doctrine = $Doctrine;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function collectionToJson($collection)
    {
        $collection_array = [];    
        foreach($collection as $key=>$item)
        {
            $item_array = [];         
            
            if(method_exists($item, 'getId')) //A cheap trick to check if its an entity. Useful for now
            {
                $reponame = get_class($item);
                $Repository = $this->Doctrine->getRepository($reponame);
                $Factory = $Repository->getFactory($this->Doctrine, $this->container); 
                $fields = $Factory->getFieldKeys();
                foreach($fields as $field)
                {
                    $item_array[$field] = $Factory->convertFieldToString($field,$item);
                }            

                $collection_array[$key] = $item_array;
            }
            elseif(is_array($item))
            {
                $collection_array[$key] = $this->collectionToJson($item);
            }
            else
            {
                if(method_exists($item, 'toArray'))
                    $collection_array[$key] = $item->toArray();
                else
                    $collection_array[$key] = $item;
            }

        }

        return $collection_array;
    } 
}

?>
